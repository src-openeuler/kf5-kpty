%undefine __cmake_in_source_build
%global framework kpty

Name:           kf5-%{framework}
Version:        5.116.0
Release:        1
Summary:        KDE Frameworks 5 Tier 2 module providing Pty abstraction

License:        BSD-3-Clause AND CC0-1.0 AND GPL-2.0-or-later AND LGPL-2.0-or-later
URL:            https://invent.kde.org/frameworks/%{framework}

%global majmin %majmin_ver_kf5
%global stable %stable_kf5

Source0:        http://download.kde.org/%{stable}/frameworks/%{majmin}/%{framework}-%{version}.tar.xz

BuildRequires:  extra-cmake-modules >= %{majmin}
BuildRequires:  kf5-rpm-macros
BuildRequires:  kf5-kcoreaddons-devel >= %{majmin}
BuildRequires:  kf5-ki18n-devel >= %{majmin}
BuildRequires:  libutempter-devel
BuildRequires:  qt5-qtbase-devel

# runtime calls %%_libexexdir/utempter/utempter
Requires:       libutempter

%description
KDE Frameworks 5 tier 2 module providing Pty abstraction.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       kf5-kcoreaddons-devel >= %{version}
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{version} -p1


%build
# find_program for utempter is failing for some reason, so
# set path explicitly to known-good value
%{cmake_kf5} \
  -DUTEMPTER_EXECUTABLE:PATH=/usr/libexec/utempter/utempter

%cmake_build


%install
%cmake_install

%find_lang %{name} --all-name --with-man


%ldconfig_scriptlets

%files -f %{name}.lang
%doc README.md
%license LICENSES/*.txt
%{_kf5_datadir}/qlogging-categories5/%{framework}.*
%{_kf5_libdir}/libKF5Pty.so.5*

%files devel
%{_kf5_includedir}/KPty/
%{_kf5_libdir}/libKF5Pty.so
%{_kf5_libdir}/cmake/KF5Pty/
%{_kf5_archdatadir}/mkspecs/modules/qt_KPty.pri


%changelog
* Mon Nov 25 2024 peijiankang <peijiankang@kylinos.cn> - 5.116.0-1
- Upgrade package to 5.116.0

* Thu Nov 21 2024 tangjie02 <tangjie02@kylinsec.com.cn> - 5.115.0-2
- adapt to the new CMake macros to fix build failure

* Mon Mar 04 2024 peijiankang <peijiankang@kylinos.cn> - 5.115.0-1
- update verison to 5.115.0

* Mon Jan 08 2024 peijiankang <peijiankang@kylinos.cn> - 5.113.0-1
- update verison to 5.113.0

* Mon Dec 12 2022 liqiuyu <liqiuyu@kylinos.cn> - 5.100.0-1
- Update package to version 5.100.0

* Mon Sep 05 2022 liweiganga <liweiganga@uniontech.com> - 5.97.0-1
- update to upstream version 5.97.0

* Tue Jul 05 2022 peijiankang <peijiankang@kylinos.cn> - 5.95.0-1
- update to upstream version 5.95.0

* Sat Feb 12 2022 peijiankang <peijiankang@kylinos.cn> - 5.90.0-1
- update to upstream version 5.90.0

* Sun Jan 16 2022 peijiankang <peijiankang@kylinos.cn> - 5.88.0-1
- update to upstream version 5.88.0

* Mon Aug 17 2020 yeqinglong <yeqinglong@uniontech.com> - 5.55.0-1
- Initial release for OpenEuler
